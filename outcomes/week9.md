# Announcements
* UX research on Rosie dashboard - Contact Sarah <cianciolas@msoe.edu> if interested
* Lab 9 updates 
    - New `ParkingLotDriver` class on lab 9 page
    - Be sure to add **DONE9** when done with lab 9
* CS1021 offerings - will teach week 6-11 of Dr. Bukowy's sections

### Class Members
* [x] Use class variables/attributes appropriately
* [x] Use class methods appropriately

### Arrays
* [x] Use an array to store primitive and object types
* [x] Create an array of a given size
* [x] Loop through an array
* [x] Pass an array as an argument

### Enhanced for loop
* [x] Design and write code that makes use of the enhanced **for** loop, a.k.a., the for-each loop

### ArrayLists
* [ ] Use an [`ArrayList<E>`](https://javadoc.taylorial.com/java.base/util/ArrayList.html) to store objects type `E`
* [ ] Use methods from the [`ArrayList<E>`](https://javadoc.taylorial.com/java.base/util/ArrayList.html) class such as `isEmpty`, `get`, `set`, `add`, `remove`, `size`, `indexOf`, and `lastIndexOf`
* [ ] Describe the advantages of an [`ArrayList<E>`](https://javadoc.taylorial.com/java.base/util/ArrayList.html) over an Array