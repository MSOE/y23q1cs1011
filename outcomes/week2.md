# Announcements
* SLM2 due tonight
* Lab 2 due Monday
* Lunch with Prof. 11:30-12:30 W, 11am R
* Lab 3, SLM3, Low stakes reading quiz, low stakes homework

### Standard Java Classes
* [x] Demonstrate the use of `String.substring()`
* [x] Demonstrate the use of `String.length()`
* [x] Demonstrate the use of `String.charAt()`
* [x] Use Oracle's [Java documentation](https://javadoc.taylorial.com/javadoc/) to ascertain if a method is part of a given class

### Arithmetic expressions
* [x] Demonstrate proper use of the following arithmetic operators: `+`, `-`, `*`, `/`, `%`
* [x] Identify and avoid unintended integer division errors
* [x] Distinguish between binary and unary operations
* [x] Define operator precedence
* [x] Interpret arithmetic expressions following operator precedence rules
* [x] Define and apply **typecasting**
* [x] Interpret code that makes use of compound assignment operations: `*=`, `/=`, `+=`, `-=`, and `%=`

### Input/Output
* [x] Use wrapper classes to perform type conversion, e.g., `int num = Integer.parseInt("14");`
* [x] Explain the source of data associated with the system input buffer: `System.in`
* [x] Perform standard/console input using the `Scanner` class
* [x] Explain the destination for data sent to the system output buffer: `System.out`
* [x] Perform standard/console output using the `System.out.println` method

### Algorithms and Design
* [x] Define the term algorithm
* [x] Explain the motivation for doing design before coding
* [x] Make use of variables and operations to perform calculations
* [x] Construct and interpret pseudocode representing sequential, conditional, and looping structures
* [x] Use flowcharts and pseudocode to describe algorithmic solutions to simple problems
* [x] Trace a program to debug it without running it