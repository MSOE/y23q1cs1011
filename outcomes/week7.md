### Defining your own classes
* [x] Create and use constructor methods
* [x] Define and use methods that have reference data types as arguments
* [x] Define and use overloaded methods
* [x] Call methods of the same class
* [x] Avoid redundant code by calling one constructor from a different constructor
* [x] Understand the implications of acting on an object to which there are multiple references
* [x] Describe the role of the garbage collector
* [x] Compare the equality of two different objects
* [x] Swap the data in two different objects

### Design Techniques
* [x] Use helper methods to avoid redundant code
* [x] Adhere to the [CS1011 coding standard](https://csse.msoe.us/cs1011/CodingStandard)
* [x] Document each method using the Javadoc convention

### Design Techniques
* [x] Simplify complicated algorithms by encapsulating subordinate tasks
* [x] Be familiar with various design approaches such as top-down, bottom-up, and case-based
* [x] Use mechanisms in IntelliJ to refactor software