# Announcements
* Hope to have lab 4 graded by 8am tomorrow
* Must follow coding standard starting with Lab 6

### Java Packages
* [x] Explain the purpose of a Java package
* [x] List at least two packages that are part of the Java standard library
* [x] Define the term fully qualified name
* [x] Explain the purpose of the `import` statement

### Object Oriented Design / Object Oriented Programming
* [x] Define the following object-oriented concepts:
    * [x] Object types (Classes)
    * [x] Class instances (Objects)
    * [x] Instance variables (Attributes/Fields)
    * [x] Instance behaviors/actions (Methods)
* [x] Distinguish between classes and objects
* [x] Describe how objects interact with one another by sending messages