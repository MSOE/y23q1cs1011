# Announcements
* Lunch with a professor, 11am Tuesday and Thursday
* Lab Quiz 2, SLM 2, Lab 2

### Writing Computer Software
* [x] Describe the steps involved in creating and running a Java program
* [x] Describe the contents of source (`.java`) and class (`.class`) files
* [x] Explain what happens (at a high level) when a Java program is compiled
* [x] Explain what happens (at a high level) when a Java program is run
* [x] Describe the difference between compilation and execution errors
* [x] Explain why a Java Virtual Machine (JVM) is required in order to run a Java program
* [x] Describe how bytecode makes Java programs portable
* [x] List the basic steps involved in software development

### Primitive datatypes, Variables, Identifiers
* [x] List the primitive types supported in Java: `int`, `long`, `float`, `double`, `char`, and `boolean` ([tutorial](http://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html))
* [x] Define **numeric literal** and provide an example of one
* [x] Define **character literal** and provide an example of one
* [x] Select the most appropriate primitive type to store a given piece of data
* [x] Use the assignment statement
* [x] Describe what happens in memory when a primitive variable is declared
* [x] Describe what happens in memory when an object identifier (reference) is declared
* [x] Describe the differences between primitives and objects (reference variables)
* [x] Define **string literal** and provide an example of one
* [x] Graphically illustrate the difference between primitives and reference types with memory diagrams
* [x] Demonstrate how an instance of a class is created (**new operator**)
* [x] Use valid and meaningful identifiers that are consistent with Java naming conventions

### Java Programming Basics
* [x] Recognize code documentation in source code
* [x] Demonstrate at least two forms of syntax for adding comments to source code
* [x] Replace hard coded constants with named constants