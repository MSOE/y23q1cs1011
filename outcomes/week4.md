### Announcements
* Exam I:
  - High: 100
  - Average: 79
  - Low: <50
* Out of town Monday - Wednesday next week
* Substitute professors: Jones, Salami
* Quiz 5 is replaced by a survey that you will need to complete

### More Standard Java Classes

* [x] Define an Application Programming Interface (API)
* [x] Use Oracle's [Java documentation](https://javadoc.taylorial.com/javadoc/) to ascertain the capabilities of a given standard java class
* [x] Use the Javadoc page for the `Math` class to perform calculations involving the following mathematic operations:
  * [x] Absolute value
  * [x] Trigonometric functions (in degrees and radians)
  * [x] _pi_ - ratio of the circumference of a circle to its diameter
  * [x] x<sup>y</sup>
  * [x] logarithmic functions
  * [x] maximum/minimum of two numbers
  * [x] Square root
* [x] Generate random numbers

### More Standard Java Classes

* [x] Be familiar with methods from the `Character` class such as `isDigit` and `toLowerCase`
* [x] Use methods from the `String` class such as `isEmpty`, `substring`, `indexOf`, etc...
