# Announcements
* Exam II Friday of next week
* I will be out of town next Monday and Tuesday
* Before lab tomorrow:
  - UML class diagram for `BuildingCostEstimator` class
  - GitHub account set up and cloned Lab 6 project

### UML
* [x] Correctly annotate and interpret methods (with arguments and return type) on a class diagram
* [x] Generating class diagram from a verbal description of a class

### Class creation basics
* [x] Describe how an object differs from a primitive
* [x] Describe how a class differs from a primitive type
* [x] Define and use classes with multiple methods and data members (fields)
* [x] Define and use value-returning and void methods
* [x] Properly use visibility modifiers in defining methods and fields
* [x] Define and use class constants
* [x] Understand and apply accessor and mutator methods
* [x] Distinguish between instance variables and local variables
* [x] Define and use instance methods and instance variables (attributes/fields)
* [x] Define and use methods that have primitive data types as arguments
* [x] Understand the importance of information hiding and encapsulation
* [x] Declare and use local variables
* [x] Describe the role of the reserved word `this`
* [x] Demonstrate use of `this` to disambiguate object fields from local variables
* [x] Trace a program including calls to class methods in multiple classes
* [x] Use the debugger to trace the execution of a multi-class program