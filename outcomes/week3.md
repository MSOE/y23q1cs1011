# Announcements
* Exam next Thursday includes content from weeks 1-3
  - May prepare 8.5" x 11" one-sided note sheet
  - Review weekly outcomes 1-3 (no switch statements)
  - Review lecture notes/videos
  - Review textbook reading and reading quizzes 1-3
  - Review homework 1-3
  - Review lab assignments 1-3
  - Review SLMs 1-3
  - Review quiz 2-4
  - Review [previous exams](https://csse.msoe.us/taylor/exams/)
  - Review slides
    + [Basics](https://csse.msoe.us/cs1011/basics.htm)
    + [Decisions](https://csse.msoe.us/cs1011/decisions.htm)
    + [Loops](https://csse.msoe.us/cs1011/decisions.htm)
* In class activities tomorrow - bring laptop

### Selection statements
* [x] Define the functionality of the following relational operators: `<`, `<=`, `!=`, `==`, `>=`, `>`
* [x] Use relational operators to control program flow
* [x] Define the functionality of the following boolean operators: `&&`, `||`, and `!`
* [x] Use boolean and relational operators to construct meaningful boolean expressions
* [x] Use boolean expressions to control program flow
* [x] Describe the behavior of an `if` statement
* [x] Describe the program flow through a series of nested `if` statements
* [x] Use nested `if` statements to control program flow
* [x] Define identifier **scope** and describe the implication to identifiers declared within an `if` block

### Selection statements
* [x] Use a `switch` statement to control program flow
* [x] Rewrite a `switch` statement with one or more (potentially nested) `if` statements
* [x] Explain the purpose of the `case`, `break` and `default` reserved words

### Iteration statements
* [x] Interpret code that makes use of the following looping constructs: `while`, `do-while`, and `for`
* [x] Design and write code that makes use of the following looping constructs: `while`, `do-while`, and `for`
* [x] Describe how the following constructs differ: `while`, `do-while`, and `for`
* [x] Rewrite a given `while` loop into an equivalent `for` loop, and vice versa
* [x] Select the most appropriate type of loop for a given problem