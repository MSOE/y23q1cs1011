package wk4;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        System.out.println("Enter two numbers");
        Scanner in = new Scanner(System.in);
        double num1 = in.nextDouble();
        double num2 = in.nextDouble();
        double diff = num1 - num2;
        double reallyBig = Double.MAX_VALUE;
        int rounded = Math.round((float)diff);
        // 1 2 => -1
        // 0.1 0.15 => -0.05
        //if(diff < 0.1 && diff > -0.1) {
        if(Math.abs(diff) < 0.1) {
            System.out.println("They are close");
        } else {
            System.out.println("The numbers you entered are more than 0.1 apart");
        }
    }
}
