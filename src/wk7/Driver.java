package wk7;

public class Driver {
    public static void main(String[] args) {
        Rectangle rec1 = new Rectangle(10, 10);
        Rectangle rec2 = new Rectangle();
        int count = 1;
        while(!rec1.equals(rec2)) {
            rec2 = new Rectangle();
            count++;
        }
        System.out.println(count);
    }
}
