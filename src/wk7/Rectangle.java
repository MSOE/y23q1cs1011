package wk7;

import java.util.Random;

public class Rectangle {
    private final int width;
    private final int height;

    private static boolean careAboutLocation = false;

    /*
    x and y represent the location of bottom left corner of the rectangle
     */
    private int x;
    private int y;


    public Rectangle() {
        Random generator = new Random();
        width = generator.nextInt(20) + 1;
        height = generator.nextInt(20) + 1;
    }

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setLocation(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static void setCareAboutLocation(boolean choice) {
        careAboutLocation = choice;
    }

    public void move(int horizontalDelta, int verticleDelta) {
        x += horizontalDelta;
        y += verticleDelta;
    }

    public int getArea() {
        return width * height;
    }

    public boolean overlaps(Rectangle that) {
        return false;
    }

    public boolean equals(Rectangle that) {
        boolean equal = true;
        if(careAboutLocation) {
            equal = this.x == that.x && this.y == that.y;
        }
        return equal && (this.width == that.width &&
                this.height == that.height);
    }
}





