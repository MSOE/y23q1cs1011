package wk3;

import java.util.Scanner;

public class InClass {

    /* Exercise 1: if
    Write a program that asks the user to enter a three-letter word. The program should
    indicate whether the letters in the word are in alphabetical order, reverse alphabetical
    order, or neither. Sample interaction should look like this:

        Enter three letters: aim
        The three letters entered are in alphabetical order.

        Enter three letters: tub
        The three letters entered are in reverse alphabetical order.

        Enter three letters: bye
        The three letters entered are not in order.
     */
    public static void main1(String[] args) {
        System.out.print("Enter three letters: ");
        Scanner in = new Scanner(System.in);
        String word = in.next();
        word = word.toLowerCase();
        if(word.charAt(0) <= word.charAt(1) && word.charAt(1) <= word.charAt(2)) {
            System.out.println("The three letters entered are in alphabetical order.");
        } else if(word.charAt(0) >= word.charAt(1) && word.charAt(1) >= word.charAt(2)) {
            System.out.println("The three letters entered are in reverse alphabetical order.");
        } else {
            System.out.println("The three letters entered are not in order.");
        }
    }

    /* Exercise 4: Loops
    Write a program that asks the user to enter a word. The program should indicate whether the
    letters in the word are in alphabetical order, reverse alphabetical order, or neither. Sample
    interaction should look like this:

        Enter a word: abhors
        The letters entered are in alphabetical order.

        Enter a word: soon
        The letters entered are in reverse alphabetical order.

        Enter a word: typical
        The letters entered are not in order.
     */
    public static void main4(String[] args) {
        System.out.print("Enter a word: ");
        Scanner in = new Scanner(System.in);
        String word = in.next().toLowerCase();
        boolean inOrder = true;
        boolean inReverseOrder = true;
        // bcbakaksisiakkdkksiaisiidifka
        for(int i=0; i<word.length()-1 && (inOrder || inReverseOrder); i+=1) {
            if(word.charAt(i) > word.charAt(i+1)) {
                inOrder = false;
            } else if(word.charAt(i) < word.charAt(i+1)) {
                inReverseOrder = false;
            }
        }
        if(inOrder) {
            System.out.println("The letters entered are in alphabetical order.");
        }
        if(inReverseOrder) {
            System.out.println("The letters entered are in reverse alphabetical order.");
        }
        if(!inOrder && !inReverseOrder) {
            System.out.println("The letters entered are not in order.");
        }
    }

    /* Exercise 8: Loops
    Suppose you are making a game that needs to control a character on the screen. The
    following key mappings should be used: h - left, j - down, k - up, l - right.
    Write a program that accepts a string of characters and generates a sequence of moves.
    Characters other than the directional characters specified earlier should be ignored.
    Sample interaction should look like this:

        jjlllk
        down
        down
        right
        right
        right
        up
     */
    public static void main8(String[] args) {
        Scanner in = new Scanner(System.in);
        String directions = in.next();
        for(int i=0; i<directions.length(); i++) {
            char letter = directions.charAt(i);
            switch (letter) {
                case 'h' -> System.out.println("left");
                case 'j' -> System.out.println("down");
                case 'k' -> System.out.println("up");
                case 'l' -> System.out.println("right");
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(starOut("*stringy"));
    }

    /*
     Returns a version of str where for every start (*) in the string
     and the chars immediately to its left and right are gone. So "ab*cd"
     yields "ad" and "ab**cd" also yields "ad".
     */
    private static String starOut(String str) {
        while(str.indexOf("*")>=0) {
            int start = str.indexOf("*")-1;
            int end = start + 2;
            while (end < str.length() && str.charAt(end) == '*') {
                end++;
            }
            str = str.substring(0, Math.max(0, start)) + str.substring(Math.min(str.length(), end + 1));
        }
        return str;
    }
}
