package wk3;

import java.util.Scanner;

public class Driver {

    public static void main3(String[] args) {
        System.out.println("How old are you now?");
        Scanner in = new Scanner(System.in);
        int age;
        do {
            System.out.println("Enter a positive number");
            age = in.nextInt();
        } while (age < 0);
        for(int i = age; i<90; i+=1) {
            System.out.println("Happy birthday, you are " + (i));
        }
        System.out.println("You exist");

    }
    // -8 -2 17 -9 -32 98
    public static void main2(String[] args) {
        System.out.println("How old are you now?");
        Scanner in = new Scanner(System.in);
        double age = in.nextDouble();
        //if (13 <= age <= 19) { ILLEGAL
        if (age < 2.0) {
            int x = 8;
            System.out.println("You're a baby");
            if(age <= 1.0) {
                System.out.println("You are " + age*12 + " months old");
            }
        } else if (age <= 5) {
            System.out.println("You're a toddler");
        } else if (age <= 12) {
            System.out.println("You're a child");
        } else if (age <= 19) {
            System.out.println("You're a teenager");
        } else {
            System.out.println("You're old");
        }
    }

    public static void main(String[] args) {
        System.out.println("Do you prefer dogs to cats? (yes/no)");
        Scanner in = new Scanner(System.in);
        String answer = in.next();
        switch (answer.toLowerCase()) {
            case "yes":
                System.out.println("dogs > cats");
                break;
            case "no":
                System.out.println("dogs <= cats");
                break;
            default:
                System.out.println("You should have entered \"yes\" or \"no\".");
        }


    }
}
