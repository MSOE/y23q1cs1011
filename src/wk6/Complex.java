package wk6;

import java.util.Scanner;

public class Complex {
    private double real;
    private double imag;

    public Complex(String line) {
        // 4.0 + 3.0i
        Scanner parser = new Scanner(line.substring(0, line.length()-1));
        // Convert the real part to a double
        real = parser.nextDouble();
        // Read in and ignore the plus symbol
        parser.next();
        // Convert the imaginary part to a double
        imag = parser.nextDouble();
    }

    public Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    public Complex plus(Complex that) {
        return new Complex(this.real + that.real, this.imag + that.imag);
    }

    public Complex minus(Complex that) {
        return new Complex(this.real - that.real, this.imag - that.imag);
    }

    public Complex times(Complex that) {
        // (a + bi)*(c + di) = ac - bd + (bc + ad)i
        return new Complex(this.real * that.real - this.imag*that.imag, this.imag*that.real + this.real*that.imag);
    }

    // TODO
    public Complex divideBy(Complex that) {
        return new Complex(this.real + that.real, this.imag + that.imag);
    }

    public String toString() {
        return "(" + real + " + " + imag + "i)";
    }
}
