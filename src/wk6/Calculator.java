package wk6;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        System.out.println("Enter two complex numbers (3.0 + 4.3i).");
        Scanner in = new Scanner(System.in);
        Complex first = new Complex(in.nextLine());
        Complex second = new Complex(in.nextLine());
        System.out.println("Enter a math operation (+, -, *, /)");
        String operation = in.next();
        if(operation.equals("+")) {
            System.out.println(first.toString() + " + " + second + " = " + first.plus(second).toString());
        } else if(operation.equals("-")) {
            System.out.println(first + " - " + second + " = " + first.minus(second));
        } else if(operation.equals("*")) {
            System.out.println(first + " * " + second + " = " + first.times(second));
        } else if(operation.equals("/")) {
//            System.out.println(first + " / " + second + " = " + first.dividedBy(second));
        }

    }
}
