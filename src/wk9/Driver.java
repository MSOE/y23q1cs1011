package wk9;

import java.util.ArrayList;

public class Driver {
    public static void main2(String[] args) {
        int[] numsArray = new int[5];
        for(int i=0; i<numsArray.length; i++) {
            numsArray[i] = i;
        }
        ArrayList<Integer> nums = new ArrayList<>();
        nums.add(numsArray[0]);
        nums.set(0, numsArray[1]);
        System.out.println(nums);
    }
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        System.out.println(words.size());
        words.add("something");
        System.out.println(words.size());
        words.add("never");
        words.add("another word");
        words.add(1, "it");
        words.remove("something");
        words.add("never");
        System.out.println(words);
        System.out.println(words.contains("never"));
        String[] wordsArray = new String[4];
        wordsArray[0] = words.get(0);
        wordsArray[1] = words.get(1);
        wordsArray[2] = words.get(2);
        wordsArray[3] = words.get(3);
        System.out.println(words.indexOf("never"));
        ArrayList<String> copy = new ArrayList<>(words);
        System.out.println(copy);

    }
}
