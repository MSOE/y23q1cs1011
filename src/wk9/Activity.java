package wk9;

import java.util.ArrayList;
import java.util.Arrays;

public class Activity {
    public static void main(String[] args) {
        boolean passed = true;
        passed = testCalculateTermQuizScore() && passed;
        passed = testCountMatches() && passed;
        passed = testGetLongWords() && passed;
        passed = testRemoveShortWords() && passed;
        passed = testUnion() && passed;
        passed = testSum28() && passed;
        passed = testAreDescending() && passed;
        passed = testCenterAverage() && passed;
        passed = testCopyFourLetterWords() && passed;
        if(passed) {
            System.out.println("All tests passed");
        }
    }

    // region Test runners
    private static boolean testCalculateTermQuizScore() {
        boolean passed = true;
        passed = testCalculateTermQuizScore(8,
                new ArrayList<>(Arrays.asList(2, 8, 8, 8, 8))) && passed;
        passed = testCalculateTermQuizScore(8,
                new ArrayList<>(Arrays.asList(2, 7, 8, 8, 9))) && passed;
        passed = testCalculateTermQuizScore(7.5,
                new ArrayList<>(Arrays.asList(2, 6, 7, 8, 9))) && passed;
        passed = testCalculateTermQuizScore(9,
                new ArrayList<>(Arrays.asList(9, 9, 9, 9, 9))) && passed;
        return passed;
    }

    private static boolean testCountMatches() {
        boolean passed = true;
        passed = testCountMatches(4,
                new ArrayList<>(Arrays.asList(2, 8, 8, 8, 8)), 8) && passed;
        passed = testCountMatches(2,
                new ArrayList<>(Arrays.asList(2, 7, 8, 8, 9)), 8) && passed;
        passed = testCountMatches(0,
                new ArrayList<>(Arrays.asList(2, 7, 8, 8, 9)), -8) && passed;
        return passed;
    }

    private static boolean testGetLongWords() {
        boolean passed = true;
        passed = testGetLongWords(0,
                new ArrayList<>(Arrays.asList("a", "a", "a"))) && passed;
        passed = testGetLongWords(3,
                new ArrayList<>(Arrays.asList("friendly", "friendly", "friendly"))) && passed;
        passed = testGetLongWords(3,
                new ArrayList<>(Arrays.asList("always", "a", "always", "always", "a"))) && passed;
        return passed;
    }

    private static boolean testRemoveShortWords() {
        boolean passed = true;
        passed = testRemoveShortWords(0,
                new ArrayList<>(Arrays.asList("a", "a", "a"))) && passed;
        passed = testRemoveShortWords(3,
                new ArrayList<>(Arrays.asList("friendly", "friendly", "friendly"))) && passed;
        passed = testRemoveShortWords(3,
                new ArrayList<>(Arrays.asList("always", "a", "always", "always", "a"))) && passed;
        return passed;
    }

    private static boolean testUnion() {
        boolean passed = true;
        passed = testUnion(6, new ArrayList<>(Arrays.asList(1, 2, 3)),
                new ArrayList<>(Arrays.asList(1, 2, 3))) && passed;
        passed = testUnion(0, new ArrayList<>(Arrays.asList(1, 2, 3)),
                new ArrayList<>(Arrays.asList(0, -2, -3))) && passed;
        passed = testUnion(6, new ArrayList<>(Arrays.asList(1, 8, 2, 3)),
                new ArrayList<>(Arrays.asList(1, 1, 2, 3))) && passed;
        return passed;
    }

    private static boolean testSum28() {
        boolean passed = true;
        passed = testSum28(true,
                new ArrayList<>(Arrays.asList(1, 2, 3, 2, 3, 2, 2))) && passed;
        passed = testSum28(false,
                new ArrayList<>(Arrays.asList(2, 2, 3, 2, 3, 2, 2))) && passed;
        passed = testSum28(false,
                new ArrayList<>(Arrays.asList(1, 1, 2, 3))) && passed;
        return passed;
    }

    private static boolean testAreDescending() {
        boolean passed = true;
        passed = testAreDescending(true,
                new ArrayList<>(Arrays.asList(82, 3, 2, -3, -12, -32))) && passed;
        passed = testAreDescending(true,
                new ArrayList<>(Arrays.asList(4, 3, 3, 2, 2, 1, 0))) && passed;
        passed = testAreDescending(false,
                new ArrayList<>(Arrays.asList(1, 1, 2, 3))) && passed;
        return passed;
    }

    private static boolean testCenterAverage() {
        boolean passed = true;
        passed = testCenterAverage(3,
                new ArrayList<>(Arrays.asList(1, 2, 3, 4, 100))) && passed;
        passed = testCenterAverage(5,
                new ArrayList<>(Arrays.asList(1, 1, 5, 5, 10, 8, 7))) && passed;
        passed = testCenterAverage(-3,
                new ArrayList<>(Arrays.asList(-10, -4, -2, -4, -2, 0))) && passed;
        passed = testCenterAverage(4,
                new ArrayList<>(Arrays.asList(5, 3, 4, 6, 2))) && passed;
        passed = testCenterAverage(4,
                new ArrayList<>(Arrays.asList(5, 3, 4, 0, 100))) && passed;
        passed = testCenterAverage(4,
                new ArrayList<>(Arrays.asList(100, 0, 5, 3, 4))) && passed;
        passed = testCenterAverage(4,
                new ArrayList<>(Arrays.asList(4, 0, 100))) && passed;
        passed = testCenterAverage(3,
                new ArrayList<>(Arrays.asList(0, 2, 3, 4, 100))) && passed;
        passed = testCenterAverage(1,
                new ArrayList<>(Arrays.asList(1, 1, 100))) && passed;
        passed = testCenterAverage(7,
                new ArrayList<>(Arrays.asList(7, 7, 7))) && passed;
        passed = testCenterAverage(7,
                new ArrayList<>(Arrays.asList(1, 7, 8))) && passed;
        passed = testCenterAverage(50,
                new ArrayList<>(Arrays.asList(1, 1, 99, 99))) && passed;
        passed = testCenterAverage(50,
                new ArrayList<>(Arrays.asList(1000, 0, 1, 99))) && passed;
        passed = testCenterAverage(4,
                new ArrayList<>(Arrays.asList(4, 4, 4, 4, 5))) && passed;
        passed = testCenterAverage(4,
                new ArrayList<>(Arrays.asList(4, 4, 4, 1, 5))) && passed;
        passed = testCenterAverage(6,
                new ArrayList<>(Arrays.asList(6, 4, 8, 12, 3))) && passed;
        return passed;
    }

    private static boolean testCopyFourLetterWords() {
        boolean passed = true;
        passed = testCopyFourLetterWords(0,
                new ArrayList<>(Arrays.asList("aim", "aimed", "a"))) && passed;
        passed = testCopyFourLetterWords(2,
                new ArrayList<>(Arrays.asList("look", "at", "that"))) && passed;
        passed = testCopyFourLetterWords(1,
                new ArrayList<>(Arrays.asList("why", "not", "shoot", "pics", "now"))) && passed;
        return passed;
    }

    // endregion

    // region Individual test method
    private static boolean testCalculateTermQuizScore(double expected, ArrayList<Integer> nums) {
        double actual = calculateTermQuizScore(nums);
        if(!compareDoubles(expected, actual)) {
            System.out.println("calculateTermQuizScore(" + nums + ") returned " + actual
                    + " but should return " + expected);
        }
        return compareDoubles(actual, expected);
    }

    private static boolean testCountMatches(int expected, ArrayList<Integer> nums, int target) {
        int actual = countMatches(nums, target);
        if(expected != actual) {
            System.out.println("countMatches(" + nums + ", " + target + ") returned " + actual
                    + " but should return " + expected);
        }
        return actual == expected;
    }

    private static boolean testGetLongWords(int expectedLength, ArrayList<String> words) {
        ArrayList<String> actual = getShortWords(words);
        if(expectedLength != actual.size()) {
            System.out.println("getShortWords(" + words + ") returned " + actual
                    + " but should return an ArrayList with " + expectedLength + " elements.");
        }
        return expectedLength == actual.size();
    }

    private static boolean testRemoveShortWords(int expectedLength, ArrayList<String> words) {
        ArrayList<String> actual = (ArrayList<String>)words.clone();
        removeShortWords(words);
        if(expectedLength != words.size()) {
            System.out.println("removeShortWords(" + words + ") returned " + actual
                    + " but should return an ArrayList with " + expectedLength + " elements.");
        }
        return expectedLength == actual.size();
    }

    private static boolean testUnion(int expectedSum, ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> actual = union(a, b);
        int actualSum = actual.stream().mapToInt(i -> i).sum();
        if(expectedSum != actualSum) {
            System.out.println("union(" + a + ", " + b + ") returned " + actual
                    + " but should return an ArrayList with elements summing to " + expectedSum);
        }
        return expectedSum == actualSum;
    }

    private static boolean testSum28(boolean expected, ArrayList<Integer> nums) {
        boolean actual = sum28(nums);
        if(expected != actual) {
            System.out.println("sum28(" + nums + ") returned " + actual
                    + " but should return " + expected);
        }
        return expected == actual;
    }

    private static boolean testAreDescending(boolean expected, ArrayList<Integer> nums) {
        boolean actual = areDescending(nums);
        if(expected != actual) {
            System.out.println("areDescending(" + nums + ") returned " + actual
                    + " but should return " + expected);
        }
        return expected == actual;
    }

    private static boolean testCenterAverage(int expected, ArrayList<Integer> nums) {
        int actual = centerAverage(nums);
        if(expected != actual) {
            System.out.println("centeredAverage(" + nums + ") returned " + actual
                    + " but should return " + expected);
        }
        return actual == expected;
    }

    private static boolean testCopyFourLetterWords(int expectedLength, ArrayList<String> words) {
        ArrayList<String> actual = copyFourLetterWords(words);
        if(expectedLength != actual.size()) {
            System.out.println("copyFourLetterWords(" + words + ") returned " + actual
                    + " but should return an ArrayList with " + expectedLength + " elements.");
        }
        return expectedLength == actual.size();
    }
    // endregion

    private static boolean compareDoubles(double expected, double actual) {
        return Math.abs(expected - actual) < 0.001;
    }

    // region Activity solutions
    // 1.
    public static double calculateTermQuizScore(ArrayList<Integer> nums) {
        int min = nums.get(0);
        int sum = 0;
        for(Integer num : nums) {
            sum += num;
            min = Math.min(min, num);
        }
        sum -= min;
        return sum/(nums.size()-1.0);
    }

    // 2.
    private static int countMatches(ArrayList<Integer> nums, int target) {
        return 0;
    }

    // 3.
    private static ArrayList<String> getShortWords(ArrayList<String> words) {
        return words;
    }

    // 4.
    private static void removeShortWords(ArrayList<String> words) {
    }

    // 5.
    private static ArrayList<Integer> union(ArrayList<Integer> a, ArrayList<Integer> b) {
        return a;
    }

    // 6.
    private static boolean sum28(ArrayList<Integer> nums) {
        return true;
    }

    // 7.
    private static boolean areDescending(ArrayList<Integer> nums) {
        return true;
    }

    // 8.
    public static int centerAverage(ArrayList<Integer> nums) {
        int min = nums.get(0);
        int max = nums.get(0);
        int sum = 0;
        for(Integer num : nums) {
            min = Math.min(min, num);
            max = Math.max(max, num);
        }
        sum -= max + min; // sum = sum - (max + min);
        return (int)Math.round(sum/(nums.size()-2.0));
    }

    // 9.
    private static ArrayList<String> copyFourLetterWords(ArrayList<String> words) {
        return words;
    }
    // endregion
}
