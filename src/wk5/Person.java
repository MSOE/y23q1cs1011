package wk5;

public class Person {
    private String firstName;
    private String middleName;
    private String lastName;

    // Constructor
    public Person(String fullName) {
        firstName = fullName.substring(0, fullName.indexOf(' '));
        middleName = "";
        lastName = fullName.substring(fullName.indexOf(' ') + 1);
    }

    public Person(String firstName, String middleName, String lastName) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }
}
