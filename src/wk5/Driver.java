package wk5;

public class Driver {
    public static void main(String[] args) {
        Person ethan = new Person("Ethan Bobby Jones");
        Person bill = new Person("Bill", "the science guy", "Nye");
        System.out.println(bill.getMiddleName());
        System.out.println(ethan.getLastName());
    }
}
