package wk8;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // Ask user to enter quiz scores
        System.out.println("How many quiz scores would you like to enter?");
        Quiz[] quizzes = new Quiz[in.nextInt()];
        System.out.println("Please enter your quiz scores in the format EARNED/POSSIBLE, e.g., 9/10");
        for(int i=0; i<quizzes.length; i++) {
            quizzes[i] = getQuiz(in);
        }

        // Find lowest quiz score
        Quiz lowest = getLowestQuiz(quizzes);

        quizzes = dropQuiz(quizzes, lowest);

        // Get total of all scores
//        int totalScores = q1.getScore();
//        totalScores += q2.getScore();
//        totalScores += q3.getScore();
//        totalScores += q4.getScore();
//        totalScores += q5.getScore();
//        totalScores += q6.getScore();
//        totalScores += q7.getScore();
//        totalScores += q8.getScore();
//
//        // Get total possible
//        int totalPossible = q1.getPossible();
//        totalPossible += q2.getPossible();
//        totalPossible += q3.getPossible();
//        totalPossible += q4.getPossible();
//        totalPossible += q5.getPossible();
//        totalPossible += q6.getPossible();
//        totalPossible += q7.getPossible();
//        totalPossible += q8.getPossible();
//
//        // Exclude lowest quiz score
//        totalScores -= lowest.getScore();
//        totalPossible -= lowest.getPossible();
//
//        // region Display quiz average
//        System.out.println("Quiz grades:");
//        displayQuiz(q1, lowest);
//        displayQuiz(q2, lowest);
//        displayQuiz(q3, lowest);
//        displayQuiz(q4, lowest);
//        displayQuiz(q5, lowest);
//        displayQuiz(q6, lowest);
//        displayQuiz(q7, lowest);
//        displayQuiz(q8, lowest);
//        System.out.println("Quiz average: " + (100.0*totalScores/totalPossible));
        // endregion
    }

    private static Quiz[] dropQuiz(Quiz[] quizzes, Quiz lowest) {
        Quiz[] newGuy = new Quiz[quizzes.length-1];
        int offset = 0;
        for(int i=0; i<newGuy.length; i++) {
            if(lowest != quizzes[i]) {
                newGuy[i] = quizzes[i+offset];
            } else {
                offset = 1;
            }
        }
        return newGuy;
    }

    private static Quiz getLowestQuiz(Quiz[] quizzes) {
        Quiz lowest = quizzes[0];
        for (Quiz quiz : quizzes) {
            if (quiz.getPercentage() < lowest.getPercentage()) {
                lowest = quiz;
            }
        }
        return lowest;
    }

    private static void displayQuiz(Quiz q1, Quiz lowest) {
        System.out.print(q1.toString());
        if(q1 == lowest) {
            System.out.println(" (dropped)");
        } else {
            System.out.println();
        }
    }

    private static Quiz getLowestQuiz(Quiz q1, Quiz q2, Quiz q3, Quiz q4, Quiz q5, Quiz q6, Quiz q7, Quiz q8) {
        if(q2.getPercentage() < q1.getPercentage()) {
            q1 = q2;
        }
        if(q3.getPercentage() < q1.getPercentage()) {
            q1 = q3;
        }
        if(q4.getPercentage() < q1.getPercentage()) {
            q1 = q4;
        }
        if(q5.getPercentage() < q1.getPercentage()) {
            q1 = q5;
        }
        if(q6.getPercentage() < q1.getPercentage()) {
            q1 = q6;
        }
        if(q7.getPercentage() < q1.getPercentage()) {
            q1 = q7;
        }
        if(q8.getPercentage() < q1.getPercentage()) {
            q1 = q8;
        }
        return q1;
    }

    private static Quiz getQuiz(Scanner in) {
        String input = in.next();
        int earned = Integer.parseInt(input.substring(0, input.indexOf('/')));
        int possible = Integer.parseInt(input.substring(input.indexOf('/')+1));
        Quiz q1 = new Quiz("Quiz 1", earned, possible);
        return q1;
    }
}
