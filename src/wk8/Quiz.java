package wk8;

public class Quiz {
    private final String name;
    private final int possible;
    private final int earned;
    private int adjustment;

    private static final int[] GRADING_SCALE = {93, 89, 85, 81, 77, 74, 70};
    private static final String[] GRADES = {"A", "AB", "B", "BC", "C", "CD", "D", "F"};
    
    private static boolean allowBrokenMath = false;
    
    public Quiz(String name, int earned, int possible) {
        this.name = name;
        this.earned = earned;
        this.possible = possible;
    }
    
    public double getPercentage() {
        return 100.0*(earned+adjustment)/possible;
    }
    
    public boolean makeAdjustment(int adjustment) {
        boolean updated = false;
        if(this.adjustment != adjustment && earned + adjustment >= 0
                && (allowBrokenMath || earned + adjustment <= possible)) {
            this.adjustment = adjustment;
            updated = true;
        }
        return updated;
    }
    
    public String getGrade() {
        String grade = "F";
        for(int i=0; grade.equals("F") && i<GRADING_SCALE.length; i++) {
            if(getPercentage()>=GRADING_SCALE[i]) {
                grade = GRADES[i];
            }
        }
        return grade;
    }

    public int getScore() {
        return earned + adjustment;
    }

    public int getPossible() {
        return possible;
    }

    public String toString() {
        return name + ": " + (earned + adjustment) + "/" + possible;
    }

    public boolean equals(Quiz that) {
        return that != null // Make sure an object was passed in
                && name.equals(that.name)
                && earned == that.earned && adjustment == that.adjustment
                && possible == that.possible;
    }

    public static void allowGreaterThan100Percent(boolean allow) {
        allowBrokenMath = allow;
    }
}
