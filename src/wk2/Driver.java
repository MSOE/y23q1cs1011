package wk2;

import java.util.Scanner;

/**
 *
 */
public class Driver {
    /** (Javadoc comment)
     * Dumb stuff in class demo
     * @param args ignore
     */
    public static void main(String[] args) {
        int pennies = 82838;
        System.out.println("dollars " + pennies/100);
        System.out.println("and " + pennies%100 + " cents");
        /*
         Block comment
         */
        final int MINUTE_PER_HOUR = 60;
        // Line comment
        Scanner in = new Scanner(System.in); // after the // is ignored
        String number = in.next();
        double x = Double.parseDouble(number);
        System.out.println(x*x);
        System.out.println(number+number);
    }
    public static void main3(String[] args) {
        String phrase = "I am a string literal not all the time, ONLY WHEN IN CLASS";
        System.out.println(phrase);
        System.out.println(phrase.length());
        System.out.println(phrase.toLowerCase());
        System.out.println(phrase.charAt(4));
        System.out.println(phrase.indexOf("literal"));
    }

    public static void main2(String[] args) {
        long j = 56786567865L;
        int i = 4;
        double x = 6.7;
        float y = (float)x;
        System.out.println(j + i * x / y);
        float z = 6.0F;
        char a = 'A';
        boolean repeat = false;
        i = (int) (x + 0.5);
        System.out.println(i);
    }
}
