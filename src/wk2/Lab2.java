package wk2;

public class Lab2 {
    public static void main(String[] args) {
        int start = 3;
        int result = negate(start);
        System.out.println("start: " + start + " result: " + result);
    }

    public static int negate(int value) {
        return -value;
    }
}
